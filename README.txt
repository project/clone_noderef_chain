This module is designed to extend the Node Clone module
(http://drupal.org/project/Node_Clone), with the features as below:

1. Clone all referenced nodes recursively.
2. Copy the files and generate new file entries in the File Field and Image
Field of the cloned node.

During cloning, this module will find all referenced node in the parent
node's "Node Reference" field, and clone them as a new child node, which 
will take the place of the original child node in the parent node. After 
that, the same procedure will be applied to the cloned child node recursively.
During the procedure, if the module finds any "File Field" or "Image Field",
it will copy the file and attach it to the cloned node. In this way, we can
avoid the case that two nodes share the same file.

This module is designed and sponsored by Red River College (www.rrc.ca)


Installation
-----

1. Enable the module
2. To clone the node recursively, go to the node page, click the "Clone" tab
3. Make sure the checkbox "Disable Chain Clone" is unselected
4. Click "Clone"
